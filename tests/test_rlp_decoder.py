# standard imports
import os
import unittest
import logging
import ctypes

# local imports
from pylibrlp import RLPEncoder

logging.basicConfig(level=logging.DEBUG)
logg = logging.getLogger()


class TestRlpDecoder(unittest.TestCase):

    def setUp(self):
        self.encoder = RLPEncoder(1024)

    
    def test_decode_single_string(self):
        b = b'\x83\x66\x6f\x6f'
        v = self.encoder.decode(b)
        self.assertEqual(v, b'foo')


    def test_decode_single_string(self):
        b = b'\xc4\x83\x66\x6f\x6f'
        v = self.encoder.decode(b)
        self.assertEqual(v, [b'foo'])


    def test_decode_multiple_adjacent(self):
        b = bytes.fromhex('f86801843b9aca008252089416cfb63467452ecf15e9d6ac48e9b4da628716c482040083666f6f26a0d825b2e797550c144f5e8acae452bd81e1947fdcbc3430240e44e967b82181f4a030856f4a51a8e9464b506490f4c7d391c386efe1134cabb546dfa4260f801c6a')
        v = self.encoder.decode(b)

        t = [
            bytes.fromhex('01'),
            bytes.fromhex('3b9aca00'),
            bytes.fromhex('5208'),
            bytes.fromhex('3102ac39709f178c0f5e87d05908609d8e09d820'),
            bytes.fromhex('0400'),
            bytes.fromhex('666f6f'),
            bytes.fromhex('01'),
            os.urandom(32),
            os.urandom(32),
        ]
        self.assertEqual(v, t)


if __name__ == '__main__':
    unittest.main()
