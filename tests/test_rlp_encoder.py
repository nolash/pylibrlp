# standard imports
import os
import unittest
import logging
import ctypes

# local imports
from pylibrlp import RLPEncoder

logging.basicConfig(level=logging.DEBUG)
logg = logging.getLogger()


class TestRlpEncoder(unittest.TestCase):

    def setUp(self):
        self.encoder = RLPEncoder(1024)


    @unittest.skip('test')
    def test_encode_single_string(self):
        v = b'foo'
        b = self.encoder.encode(v)
        print(b.hex())


    @unittest.skip('test')
    def test_encode_single_list(self):
        v = [b'foo']
        b = self.encoder.encode(v)
        print(b.hex())


    @unittest.skip('test')
    def test_encode_nested(self):
        v = [b'foo', [b'bar', b'baz']]
        b = self.encoder.encode(v)
        print(b.hex())


    def test_encode_multiple_adjacent(self):
        v = [
            bytes.fromhex('01'),
            bytes.fromhex('3b9aca00'),
            bytes.fromhex('5208'),
            bytes.fromhex('3102ac39709f178c0f5e87d05908609d8e09d820'),
            bytes.fromhex('0400'),
            bytes.fromhex('666f6f'),
            bytes.fromhex('01'),
            os.urandom(32),
            os.urandom(32),
        ]
        b = self.encoder.encode(v)
        print(b.hex())


if __name__ == '__main__':
    unittest.main()
